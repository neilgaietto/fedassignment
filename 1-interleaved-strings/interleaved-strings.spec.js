describe("interleaved strings suite", function() {
    it("base expectation", function() {
      expect(util.interleaveStrings('abc', '123')).toBe("a1b2c3");
    });
    it("long strings", function() {
      expect(util.interleaveStrings('abcdefghij', '1234567890') ).toBe("a1b2c3d4e5f6g7h8i9j0");
    });
    it("long a and short b", function() {
      expect(util.interleaveStrings('abcde', '123') ).toBe("a1b2c3de");
    });
    it("short a and long b", function() {
      expect(util.interleaveStrings('ab', '12345') ).toBe("a1b2345");
    });
    it("string and string", function() {
      expect(util.interleaveStrings('abc', '0')).toBe("a0bc");
    });
    it("string and non-string", function() {
      expect(util.interleaveStrings('abc', 1)).toBe("abc");
    });
    it("non-string and string", function() {
      expect(util.interleaveStrings(1, '123')).toBe("123");
    });
    it("empty and string", function() {
      expect(util.interleaveStrings('', '123')).toBe("123");
    });
    it("null and string", function() {
      expect(util.interleaveStrings(null, '123') ).toBe("123");
    });
    it("empty and null", function() {
      expect(util.interleaveStrings('', null)).toBe("");
    });
    it("null and null", function() {
      expect(util.interleaveStrings('', null)).toBe("");
    });
  });



