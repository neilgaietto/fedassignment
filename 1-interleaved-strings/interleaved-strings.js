var util = (function () {

    //interleave characters. note: if one string is longer then it will still continue filling with the longer string
    var interleaveStrings = function (strA, strB) {

        //to simplify, if either string is not a string then just make it an empty string
        strA = (typeof (strA) === 'string' && strA.length) ? strA : '';
        strB = (typeof (strB) === 'string' && strB.length) ? strB : '';


        var out = '';
        for (var i = 0; i < Math.max(strA.length, strB.length); i++) {
            if (strA.length > i) {
                out += strA[i];
            }
            if (strB.length > i) {
                out += strB[i];
            }
        }
        return out;
    }

    return { interleaveStrings: interleaveStrings };
})();
