//RateProviders: contains the various data providers. Each provider contains get and mapping details
var RateProviders = (function () {
    var providerA = {
        name: "Provider A",
        getRatesUrl: "http://providera.com/api/getData",
        reportMapper: function (ratesData) {
            //transform api data and map data into report model
            return { provider: this.name, rates: ratesData };
        }
    }
    var providerB = {
        name: "Provider B",
        getRatesUrl: "http://providerb.com/api/getData",
        reportMapper: function (ratesData) {
            //transform api data and map data into report model
            return { provider: this.name, rates: ratesData };
        }
    }

    //providers contain a url to get data and a mapping function to normalize the data for charting
    return {
        providerA: providerA,
        providerB: providerB
    };
})();

//RateService: Use to pull rate data based on the passed in provider
var RateService = (function () {
    var getProviderRates = function (provider) {
        var rateData = getData(provider.getRatesUrl);
        var reportData = provider.reportMapper(rateData);
        return reportData;
    }

    var getProviderRatesList = function (providerList) {
        return providerList.map(x => getProviderRates(x));
    }

    var getData = function (url) {
        //make ajax request to pull rate data
        return [ //return test data to simulate
            { name: 'example 1 month', term: 1, rate: 0.004969 },
            { name: 'example 2 months', term: 2, rate: 0.006105 },
            { name: 'example 3 months', term: 3, rate: 0.007776 },
            { name: 'example 6 months', term: 6, rate: 0.011442 },
            { name: 'example 12 months', term: 12, rate: 0.014546 }]
    }

    return {
        getProviderRates: getProviderRates,
        getProviderRatesList: getProviderRatesList
    };
})();
