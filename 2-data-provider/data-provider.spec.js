describe("Rate Providers suite", function() {
    it("it has providers", function() {
      expect(Object.values(RateProviders)).toBeDefined();
    });
    it("provider has a name", function() {
      expect(RateProviders.providerA.name).toBeDefined();
    });
    it("provider has a rates getRatesUrl", function() {
      expect(RateProviders.providerA.name).toBeDefined();
    });
    it("provider has a report mapper", function() {
      expect(RateProviders.providerA.reportMapper).toBeDefined();
    });
    it("provider adds name to report mapping", function() {
      expect(RateProviders.providerA.reportMapper({}).provider).toBe(RateProviders.providerA.name);
    });
  });



  describe("Rate Service suite", function() {
    it("it receives a provider", function() {
      expect(RateService.getProviderRates(RateProviders.providerA)).toBeDefined();
    });
    it("it uses report mapper", function() {
      spyOn(RateProviders.providerA,'reportMapper');
      RateService.getProviderRates(RateProviders.providerA)
      expect(RateProviders.providerA.reportMapper).toHaveBeenCalled();
    });
    it("it returns rate data", function() {
      expect(RateService.getProviderRates(RateProviders.providerA).rates).toBeDefined();
    });
    it("it returns the provider name", function() {
      expect(RateService.getProviderRates(RateProviders.providerA).provider).toBeDefined();
    });
    it("it returns a list of provider rates", function() {
      expect(RateService.getProviderRatesList(Object.values(RateProviders))).toBeDefined();
    });
  });


