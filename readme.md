# FED Assignment

Completed the 3 required questions and #5 as the 'pick one' question.
All of these were completed with plain js and html. Libraries were only included when needed via CDN.
Tests were built using Jasmine standalone

1. Found in `1-interleaved-strings/interleaved-strings.js`
    - View tests: `1-interleaved-strings/tests.html`
    - View test specs: `1-interleaved-strings/interleaved-strings.spec.js`
2. Found in `2-data-provider/data-provider.js`
    - JS implementation of a provider store and rates service. Hopefully this was close to what you were looking for.
    - The provider store functions as a data source for the various providers and maps to a normalized format
    - The rate service takes in a provider and builds the data.
    - View tests: `2-data-provider/tests.html`
    - View test specs: `2-data-provider/data-provider.spec.js`
3. Found in `3-signup-form/index.html`
    - Html markup for a semantic html web form. CSS not included.
4. (skipped for #5)
5. Found in `5-promises/index.html` with assets in the same directory
    - Choose #5 as the 'pick one' question.
    - Html page with Bluebird and jQuery CDN resources pulled in.
    - Executes instructions on load.
    - Important: This will not work if you are loading via a "file://" path since its making ajax calls to files within its own directory. I use the 'Live Server' vs code extension to run.